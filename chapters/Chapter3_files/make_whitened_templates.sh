H1bank=bns_inj_1.xml
psd=H1L1V1-REFERENCE_PSD-1186624818-511200.xml.gz
ifo=H1
tmpltid=0

gstlal_spiir_plot_waveform --output-item all --output-prefix ${ifo}_tmplt${tmpltid} --template-bank $H1bank --template-bank-type sim --flow 20 --reference-psd $psd --instrument ${ifo} --template-id $tmpltid -v --samples-max-64 2048 --autocorrelation-length 351 --samples-max-256 1024 --samples-max 2048 --approximant SpinTaylorT4 --waveform-domain FD --dump-txt --sampleRate 2048.0 --epsilon-min 0.019



