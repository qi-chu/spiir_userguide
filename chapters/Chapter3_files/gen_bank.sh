# generate SPIIR coefficients for this extracted bank
PSD="/fred/oz016/chichi/review/gstlal_H1L1V1-REFERENCE_PSD-1186624818-687900.xml.gz"
APPROX="SpinTaylorT4" # always set to this if lowmass
NEGLAT=0 # set to 0 latency 
IFO="H1" # detector PSD you want to use
outbank=joel_bank/iir_${IFO}-${tmplt_idx}.xml.gz

gstlal_iir_bank --reference-psd ${PSD} --template-bank $extract_bank --flow 20.0 --waveform-domain FD --padding 1.3 --instrument ${IFO} --output $outbank --autocorrelation-length 351 --sampleRate 2048.0 -v --epsilon-options '{"epsilon_start":1.0,"nround_max":20,"initial_overlap_min":0.95,"b0_optimized_overlap_min":0.95,"epsilon_factor":1.2,"filters_max":350}' --optimizer-options '{"verbose":true,"passes":16,"indv":true,"hessian":true}' --approximant ${APPROX} --negative-latency ${NEGLAT}


