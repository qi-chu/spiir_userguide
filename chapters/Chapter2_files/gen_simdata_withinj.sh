BNS_INJECTION_1=../bns_inj_1.xml
event_time=$1
START=$(( $event_time - 1000 ))
STOP=$(( $event_time + 200 ))
path=gaussian_inj
gstlal_fake_frames --data-source AdvLIGO --channel-name=H1=FAKE-STRAIN --frame-type=H1_FAKE --gps-start-time $START --gps-end-time $STOP --output-path=$path --verbose --injections $BNS_INJECTION_1
gstlal_fake_frames --data-source AdvLIGO --channel-name=L1=FAKE-STRAIN --frame-type=L1_FAKE --gps-start-time $START --gps-end-time $STOP --output-path=$path --verbose --injections $BNS_INJECTION_1
gstlal_fake_frames --data-source AdvVirgo --channel-name=V1=FAKE-STRAIN --frame-type=V1_FAKE --gps-start-time $START --gps-end-time $STOP --output-path=$path --verbose --injections $BNS_INJECTION_1

