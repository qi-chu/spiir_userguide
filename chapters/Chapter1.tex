\chapter{Installation}

\section{SPIIR v0.1}

SPIIR v0.1 refers to the reviewed version (tag spiir-O3-v6) for the O3 online production run which use the gstreamer-0.10.x packages. 

The most easy way to install SPIIR package is to use Patrick's one-click installation script on either CIT or OzStar clusters. Installation on personal servers is not recommended due to too many prerequisite dependencies. 

\subsection{Apply for OzStar, CIT or Pawsey account}

To apply for an account of the OzStar cluster, go to the link \footnote{\url{https://supercomputing.swin.edu.au/docs/1-getting_started/Accounts.html}}. Please fill in with the line manager name ``Linqing Wen" and their contact number. After you submitted the application, you should get approved by the OzStar team in 48hrs. After that, you also need to apply to join the existing SPIIR group project ``oz016" using the same link above. 

To apply for an account of the CIT cluster, please contact Linqing to become a LIGO member first. Then fill in the LIGO membership registration link \footnote{\url{https://my.ligo.org}}. After you've got the membership, click the "Apply/Manage a LDG account" in the menu of the link and submit your application. You will receive instructions to log into any cluster of the LIGO Data Grid (LDG) system across the world.

To apply for Pawsey account, contact Linqing, Fiona or Chayan to send you an invitation email.

\subsection{Automated installation}
The instructions of Patrick's one-click automated installation can be found at the induction manual\footnote{\url{https://git.ligo.org/alex.codoreanu/gwdc_utils}}. 

\subsection{Installation with shared dependency}
The old way of installation is to use shared dependencies on a cluster via environment settings. Here is an example of settings to use the SPIIR dependencies on CIT:
\begin{lstlisting}
unset PKG_CONFIG_PATH
unset PYTHONPATH
export LD_LIBRARY_PATH=/usr/lib64
export ACLOCAL_PATH=/home/spiir/opt/misc/share/aclocal:$ACLOCAL_PATH
export USE_LOCATION=/home/spiir/opt/

. /home/spiir/.pythonextra
. /home/spiir/.misc
. /home/spiir/.cudarc
. /home/spiir/.lalsuiteuprc

GSTREAMER_PATH=/home/spiir/opt/gstreamer

PATH=${GSTREAMER_PATH}/bin:$PATH
PKG_CONFIG_PATH=${GSTREAMER_PATH}/lib/pkgconfig:$PKG_CONFIG_PATH
LD_LIBRARY_PATH=${GSTREAMER_PATH}/lib:$LD_LIBRARY_PATH
PYTHONPATH=${GSTREAMER_PATH}/lib64/python2.7/site-packages:$PYTHONPATH
GST_PLUGIN_PATH=${GSTREAMER_PATH}/lib/gstreamer-0.10:$GST_PLUGIN_PATH
export INTROSPECTION_COMPILER_OPTS="--includedir=/home/spiir/opt/misc/share/gir-1.0"

export PATH PKG_CONFIG_PATH PYTHONPATH GST_PLUGIN_PATH LD_LIBRARY_PATH
\end{lstlisting}

Users on OzStar within the same project group can share installed softwares via environment settings. You need to be in oz016 group to use the softwares. 
\begin{lstlisting}
module load gcc/6.4.0 openmpi/3.0.0
module load cuda/9.0.176
module load lapack/3.8.0
module load gsl/2.4
module load fftw/3.3.7
module load framel/8.30
module load metaio/8.4.0
module load cfitsio/3.420
module load numpy/1.14.1-python-2.7.14
module load scipy/1.0.0-python-2.7.14
module load swig/3.0.12-python-2.7.14
module load git/2.16.0
module load hdf5/1.10.1-serial
module load h5py/2.7.1-python-2.7.14-serial
module load astropy/2.0.3-python-2.7.14

# project wide gstreamer, framecpp
export SCR=/fred/oz016
export PATH=${SCR}/opt/bin:${PATH}
export LIBRARY_PATH=${SCR}/opt/lib:${LIBRARY_PATH}
export LD_LIBRARY_PATH=${SCR}/opt/lib:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${SCR}/opt/lib/pkgconfig:${PKG_CONFIG_PATH}
export PYTHONPATH=${SCR}/opt/lib/python2.7/site-packages:${PYTHONPATH}
export GST_PLUGIN_PATH=${SCR}/opt/lib/gstreamer-0.10:${GST_PLUGIN_PATH}

# project wide lalsuite
export PATH=${SCR}/opt-pipe/bin:${PATH}
export LIBRARY_PATH=${SCR}/opt-pipe/lib:${LIBRARY_PATH}
export LD_LIBRARY_PATH=${SCR}/opt-pipe/lib:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${SCR}/opt-pipe/lib/pkgconfig:${PKG_CONFIG_PATH}
export PYTHONPATH=${SCR}/opt-pipe/lib/python2.7/site-packages:${PYTHONPATH}
export LAL_DATA_PATH=${SCR}/opt-pipe/share/lalsimulation:${LAL_DATA_PATH}

export OMP_NUM_THREADS=1
\end{lstlisting}

After setting up the dependent libraries via the above settings, we need to set up the environment settings for the to-be-installed SPIIR packages. The SPIIR settings (referred to as your\_spiir\_setting) is as follows:
\begin{lstlisting}
INSTALLPATH=your_location
PATH=${INSTALLPATH}/bin:$PATH
PKG_CONFIG_PATH=${INSTALLPATH}/lib/pkgconfig:$PKG_CONFIG_PATH
LD_LIBRARY_PATH=${INSTALLPATH}/lib:${INSTALLPATH}/lib64:$LD_LIBRARY_PATH
PYTHONPATH=${INSTALLPATH}/lib64/python2.7/site-packages:$PYTHONPATH
GST_PLUGIN_PATH=${INSTALLPATH}/lib/gstreamer-0.10:${GST_PLUGIN_PATH}

export PATH PKG_CONFIG_PATH PYTHONPATH GST_PLUGIN_PATH LD_LIBRARY_PATH
\end{lstlisting}

After install or set up the dependencies, the SPIIR package can be installed by the three following steps. 
\begin{lstlisting}
# step1: install gstlal package:
cd gstlal
./00init.sh
./configure --prefix=$INSTALLPATH
make && make install
cp gst/lal/.libs/libgstlal.so $your_location/lib/gstreamer-0.10/
source $your_spiir_setting

# step2: install gstlal-ugly package:
cd gstlal-ugly
./00init.sh
./configure --prefix=$INSTALLPATH
make && make install
source $your_spiir_setting

# step3: install gstlal-spiir package:
cd gstlal-spiir
./00init.sh
./configure --prefix=$your_gstlal_installation_path --with-cuda=$cuda_path
make && make install
source $your_spiir_setting
\end{lstlisting}

You can check if you successfully installed the above three packages by
\begin{lstlisting}
gst-inspect |grep gstlal
gst-inspect |grep ugly
gst-inspect |grep cuda
\end{lstlisting}
If no command returned error msgs, the three packages have been installed properly. Otherwise, send the error msg to the developers.

\subsection{Installation from scratch}
It is not recommended to install the SPIIR package from scratch, only if you want to upgrade the SPIIR package on new dependencies. Below are the requirements.
\subsubsection{Hardware requirement}
A GPU needs to be installed to use the SPIIR package, either it be the old Kepler GPU, or the latest Volta GPU. 
\subsubsection{Software requirement}
The SPIIR package needs at least the following dependent libraries:
\begin{lstlisting}
# General libraries:
gcc/6.4.0
cuda/6.5 to 10.0
lapack/3.6.1 or 3.8.0
gsl/1.16 or 2.4
fftw/3.3.7
framel/8.30
metaio/8.4.0
cfitsio/3.420
numpy/1.9.1 or 1.14.1
scipy/0.14.0 or 1.0.0
python/2.7.5 or 2.7.14

# Gstreamer-related libraries:
glib/2.29.92
pygobject/2.21.3
gstreamer/0.10.36
gst-plugins-base/0.10.36
gst-plugins-good/0.10.31
gst-plugins-ugly/0.10.19
gst-plugins-bad/0.10.23
gst-python/0.10.22

# LIGO scientific collaboration libraries:
ldas-tools/2.4.2
lalsuite/6.15.0+
GraceDB client/2.2.0
\end{lstlisting}


\section{SPIIR v1.0}
\subsection{Aim}

The current SPIIR package depends on gstreamer 0.10.36 and gstlal 0.x libraries (note that the gstlal library is an extension to the gstreamer library). Both gstreamer and gstlal libraries have been upgraded to 1.x. There is a need to port the entire SPIIR package to use the latest maintained 1.x libraries. The goal for the master students is to port the SPIIR pipeline described in Chapter 5.2 to gstreamer 1.x and gstlal 1.x libraries, and port the entire SPIIR package if time permits.

\subsection{Background} 
The guide for porting gstreamer 0.10 to gstreamer 1.0 can be found below\footnote{\url{https://gstreamer.freedesktop.org/documentation/application-development/appendix/porting-1-0.html?gi-language=c}}.

\subsection{Proposed milestones}
Milestone 1:
\begin{enumerate}
\item find the installed versions of gstreamer 1.x on OzStar. Set up environment to use that.
\item install the latest gstlal 1.x libraries from the source code repository (\href{https://git.ligo.org/lscsoft/gstlal}{this link}). Set up environment to use that.
\item test on a simple gstlal pipeline "gstlal\_play" explained in Sec.~\ref{sec:data_whiten} using the installed gstreamer 1.x and gstlal 1.x settings mentioned above.
\end{enumerate}

Milestone 2:
\begin{enumerate}
\item tests to make sure the gstreamer 1.x and gstlal 1.x plugins in Tab.~\ref{tab:spiirfilt_codebase}  work.
\item port the SPIIR plugin codes and the SPIIR pipeline in Tab.~\ref{tab:spiirfilt_codebase} one by one.
\item Test on the pipeline of Sec.~\ref{sec:spiir_pipe}.
\end{enumerate}

Challenge Milestone 3:
\begin{enumerate}
\item tests to make sure the gstreamer 1.x and gstlal 1.x plugins in Tab.~\ref{tab:whole_codebase} work.
\item port the SPIIR plugin codes and the SPIIR pipeline in Tab.~\ref{tab:whole_codebase} one by one.
\item Test on the pipeline of Sec.~\ref{sec:pipe_offline}.
\item port the entire gstlal-spiir package to gstreamer 1.x and gstlal 1.x.
\item set up the rpm release for the gstlal-spiir package.
\end{enumerate}


