\chapter{Data generation}

This chapter will show how to generate an injection file which include a list of injection parameters that can be injected into data (Sec.~\ref{sec:an_injection_set}). Sec.~\ref{sec:emp_data_inj} shows how to generate silent data, with injected waveforms from an injection file. Sec.~\ref{sec:gauss_data_inj}) shows how to generate Gaussian noise data from advanced LIGO/Virgo noise power spectral density (PSD) with or without injections. Sec.~\ref{sec:real_data shows how to find real data on LIGO clusters. Sec.~\ref{sec:psd} shows how to measure the PSD from existing data, whether it Gaussian or real data. Sec.~\ref{sec:data_whiten} shows the program to whiten the data and output it to a csv format.

\section{An injection set}
\label{sec:an_injection_set}
\subsection{Injection file format -- LIGOLW XML format}
\label{sec:ligolw_format}
The default file format to store a set of injections is the LIGO lightweight (LIGOLW) XML data format, similar to the HTML format. The definition of this format can be found here\footnote{\url{https://dcc.ligo.org/LIGO-T980091}}. As the legacy \verb;glue; package or the latest \verb;ligo.lw; package would be installed as one of the SPIIR package dependencies, you should be able to use ``ligolw\_print" command to print parameters for all injections in the injection file. See example at the next section. The SPIIR pipeline recognizes this LIGOLW format and can insert the injections into real data or simulated data.

\subsection{Injection file generation}
An injection file contains a table/list/set of injections. For each injection, it lists the end time of this injection on each detector in GPS second and nanosecond, the intrinsic parameters and other properties. The following is a ``Makefile.inj" file that can generate an injection file ``bns\_inj\_1.xml" using the "lalapps\_inspinj" command. Run ``make -f Makefile.inj" and it output an injection file with injections starting from Aug 13st, 2:0:0 2017 UTC with 3000 seconds apart. "lalapps\_inspinj" can also use a noise PSD to generate injections at given signal-to-noise ratios (SNRs). To check out all available options to construct the injections, run "lalapps\_inspinj --help".

\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/Makefile.inj}

Use ``ligolw print? command to print parameters for the injections. ``-t" inputs the table name, for an injection file, it must be ``sim\_inspiral". ``-c" inputs the column names, e.g. here print the end times of individual detectors, masses, and effective distances of the source:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/print_paras.sh}

The parameters for the first injection of ``bns\_inj\_1.xml" after execution of the above command is:
\begin{table}[htb]
\caption[]{"h\_end\_time" and "h\_end\_time\_ns" are the end time of the injection in GPS seconds and GPS nano seconds in LIGO-H detector. "alpha4" is the computed expected SNR in LIGO-H detector, as explained in the last section of this chapter.}
 \resizebox{\textwidth}{!}{% use resizebox with textwidth
  \begin{tabular}{@{} |ccccccccccccc| @{}}

    \hline
     & h\_end\_time & h\_end\_time\_ns & mass1 & mass2 & mchirp & eta & spin1z & spin2z & eff\_dist\_h & alpha4 & longitude  & latitude \\ 
    \hline
    bns\_inj\_1.xml & 1186624818 & 12168969 & 2.93 & 1.81 & 1.99 & 0.24 & -0.10 & -0.32 & 32.5 & 41.0 & 1.99 & -0.38 \\ 
    \hline
  \end{tabular}
}
\label{tab:inj1_prop}
 \end{table}

\subsection{Calculate expected SNR for injections}

Use ``gstlal\_inspiral\_injection\_snr" to calculate expected SNR for each injection and rewrite the expected SNRs into the injection file. The expected SNRs for LIGO-H, LIGO-L, and Virgo will be rewritten in the fields of ``alpha4", ``alpha5", and ``alpha6" of the injection file, e.g. ``bns\_inj\_1.xml" in the following example. 
\begin{lstlisting}
gstlal_inspiral_injection_snr --injection-file bns_inj_1.xml --flow 20.0 --reference-psd-cache psd.cache
\end{lstlisting}

This script reads a given cache file (psd.cache) which is just a csv file that lists the locations of all PSD files. PSD files are in LIGOLW XML format. There could be more than one PSD file in the cache as for each injection at a different time, we can read the PSD for that particular time for the SNR calculation. In this example, the content of the cache file is shown below:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/psd.cache}


\section{Empty data with injections}
\subsection{Data format}
Simulated data or real data are stored in files with the ``.gwf" format defined here\footnote{\url{https://dcc.ligo.org/LIGO-T970130/public}}. As the \verb;framel; and the \verb;lalsuite; packages would be installed as dependencies for the SPIIR package, you should be able to use ``FrChannels" to print out the channels of a frame file and use ``FrCheck" to list the number of frames in that file.

\label{sec:emp_data_inj}
Use ``gstlal\_fake\_frames" to generate fake data with options like silence, white Gaussian, or colored Gaussian and with or without injections. The output will be a directory given by "--output-path" with frame files in it. Need to specify channel name, frame type and GPS times.

\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/gen_zerodata_withinj.sh}

\section{Simulated Gaussian advanced LIGO noise with or without injections}
\label{sec:gauss_data_inj}
Use "gstlal\_fake\_frames" to generate simulated advanced LIGO noise data with or without injections. The output will be a directory given by "--output-path" with frame files in it. E.g.:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/gen_simdata_withinj.sh}

\section{Real data}
\label{sec:real_data}
On any of the cluster of LIGO Data Grid, you should be able to use ``gw\_data\_find" on a to find real offline data. The output will be a cache file (a CSV file) that lists the duration and locations of the frame files. The online/real-time data is transferred and distributed to shared-memory of computing nodes through \verb;llldd; or \verb;kafka; or both mechanisms. C00 offline data is equivalent to the online data that have been used in the online runs. Here is the script that can find various versions of data:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/datatypes.sh}

\section{Estimation and plotting the PSD of the data}
\label{sec:psd}
``gstlal\_reference\_psd is the program to estimate the median PSD of data given a period of time. It uses the lal\_whiten plugin to estimate the PSD and out put the PSD to a LIGOLW XML file. An example to use this command is shown below:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/make_psd.sh}

``gstlal\_plot\_psd" can be used to plot the estimated PSD in PNG forma. This program also calculates the horizon distance for each PSD and mark it in the plot. The command to use this program is shown here:
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/plot_psd.sh}

The output plot from this command is shown below.
\begin{figure}[!h]
\centering
\includegraphics[width=.9\textwidth]{chapters/Chapter2_files/review_psd.png}
\caption[]{PSD plot from estimated PSD using gstlal\_reference\_psd program. The legend shows the horizon distance for each detector, LIGO-H (H), LIGO-L (L), and Virgo (V).}
\label{fig2:psd}
\end{figure}

\section{Data whitening}
By default, the conventional FFT method is used for whitening (lal\_whiten plugin). This introduce a delay for collection of data. The time-domain FIR whitening (lal\_tdwhiten plugin) currently has two issues, one is generating many spikes, the other is the phase issue.

"gstlal\_play" is a simple program that takes any data, either simulated or real, in frame format and perform simple optional functions including high pass filtering, low pass filtering, and whitening. At the end, it outputs the processed data in CSV format. It is also worth mentioning that the SPIIR pipeline performs whitening at the beginning that the whitened data can be output into CSV format from the detection pipeline. See Sec.~\ref{sec:pipe_dump} for more details.

\subsection{Whitening using gstlal\_play}
\label{sec:data_whiten}
Use "gstlal\_play" command to whiten the data. 
\lstinputlisting[language=Bash, style=alexstyle]{chapters/Chapter2_files/play_whiten.sh}

The output of the above script is a text file with two columns. The first column lists timestamps for each sample and the second lists samples of whitened H1 data. A simple python script can plot this txt file. The figure below shows the whitened data for H1, L1, and Virgo detectors.

\begin{figure}[!h]
\centering
\includegraphics[width=.9\textwidth]{chapters/Chapter2_files/wdata_0.png}
\caption[]{Whitened data for LIGO-H (top), LIGO-L (middle), and Virgo (bottom) using gstlal\_play,. The original data contains only an injection without any noise. The whitening uses realist PSDs for each detector. The injected signal suffers the most after Virgo PSD whitening due to that the Virgo noise PSD is the worst of the three.}
\label{fig2:wdata}
\end{figure}

\subsection{Whitening as part of the detection pipeline}
\label{sec:pipe_dump}
The current detection pipeline "gstlal\_inspiral\_postcohspiir\_online" provides the option to dump intermediate data products including the data after whitening process. To do that, just simply add "--nxydump-segment GPS\_START\_TIME:GPS\_END\_TIME" to the options. Each data product consists of one timestamp column and one or several columns for sample values. The data products from this options include:
\begin{enumerate}
    \item before\_highpass\_data\_IFO1\_GPSSTARTTIME.dump. This shows the raw data that has been applied segment vetoes.
    \item If the pipeline has been input a FIR whitening option (--fir-whitener 1),  two products will be output: after\_highpass\_data\_IFO1\_GPSSTARTTIME.dump showing data after the highpass and after\_tdwhiten\_data\_IFO1\_GPSSTARTTIME.dump showing data after the tdwhitener function following highpass. If the pipeline uses the FFT whitening (default or --fir-whitener 0), there is output after\_fdwhiten\_data\_IFO1\_GPSSTARTTIME.dump. This shows the data that has been whitened using FFT with the block size given by the --psd-fft-length option.
    \item after\_htgate\_data\_IFO1\_GPSSTARTTIME.dump. This shows the data that have been whitened, either td or fd, been applied the segment vetoes again and been zeroed-out for any spikes above the --ht-gate-threshold.
    \item snr\_gpu\_GSPSTARTTIME\_IFO1\_GPUSTREAMID.dump. Following the last process, this shows data been filtered with a SPIIR bank where GPUSTREAMID is the GPU stream ID allocated for this bank job. 
\end{enumerate}
Words in upper cases are explained here: IFO is one of the H, L or V. GPSSTARTTIME is the GPS\_START\_TIME set at the option, The data product after\_htgate\_data\_IFO1\_GPSSTARTTIME.dump will be used in Sec.~\ref{sec:mf_fft} for a standalone matched filtering program.
    






