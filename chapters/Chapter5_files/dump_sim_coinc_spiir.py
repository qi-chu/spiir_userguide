#!/usr/bin/env python
#
# 
# Copyright (C) 2019 Qi Chu
# modified from
# Copyright (C) 2009-2013  Kipp Cannon, Chad Hanna, Drew Keppel
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

### A program to select and dump candidates from a sql file

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#

import pdb
from optparse import OptionParser
import os
import scipy
import sqlite3
import sys

import lal
from lal.utils import CacheEntry

from glue import iterutils
from ligo import segments
from glue.ligolw import dbtables
from glue.ligolw import lsctables
from glue.ligolw.utils import segments as ligolw_segments
import csv

#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#
#

def create_sim_coinc_view(connection, name):
	"""
	Construct a sim_inspiral --> best matching coinc_event mapping.
	Only injections that match at least one postcoh candidates will get an entry in this
	table. If there are more than one postcoh candidates, select the one with the highest coherent SNR
	the output sim_coinc_map table is like this:
		simulation_id, event_id, far
		sim_inspiral:simulation_id:32135,postcoh:event_id:241023,1.3e-7
		sim_inspiral:simulation_id:32155,postcoh:event_id:250000,4.5e-3
		note the simulation_id and event_id may not be continous
	"""
	cursor = connection.cursor()
	cursor.execute("""
CREATE TABLE
	sim_coinc_map
AS
	SELECT
		sim_inspiral.simulation_id AS sim_id,
		(
			SELECT
				a.coinc_event_id
			FROM
				coinc_event_map AS a
				JOIN postcoh ON (
					postcoh.event_id == a.coinc_event_id
				)
			WHERE
				a.table_name == 'sim_inspiral'
				AND a.event_id == sim_inspiral.simulation_id
			ORDER BY
				postcoh.cohsnr DESC
			LIMIT 1
		) as coinc_event_id,
		(
			SELECT
				postcoh.far
			FROM
				coinc_event_map AS a
				JOIN postcoh ON (
					postcoh.event_id == a.coinc_event_id
				)
			WHERE
				a.table_name == 'sim_inspiral'
				AND a.event_id == sim_inspiral.simulation_id
			ORDER BY
				postcoh.cohsnr DESC
			LIMIT 1
		) as far
		FROM
		sim_inspiral
	WHERE
		coinc_event_id IS NOT NULL
	""")

	# construct another table sim_sngl_far with the features one is interested
	# by tracing table sim_coinc_map

	cursor.execute("""
CREATE TABLE
	sim_sngl_far
	AS
		SELECT
			sim_inspiral.simulation_id,
			sim_coinc_map.far,
			sim_inspiral.mchirp,
			sim_inspiral.eff_dist_l,
			postcoh.ifos,
			postcoh.cohsnr,
			postcoh.snglsnr_H,
			postcoh.snglsnr_L,
			postcoh.snglsnr_V,
			postcoh.chisq_H,
			postcoh.chisq_L,
			postcoh.chisq_V,
			postcoh.mchirp,
			sim_inspiral.h_end_time,
			postcoh.far_h,
			postcoh.far_l,
			postcoh.far_v,
			postcoh.mass1,
			postcoh.mass2,
			postcoh.rank,
			postcoh.nullsnr,
			postcoh.end_time_H,
			postcoh.end_time_ns_H,
			postcoh.end_time_L,
			postcoh.end_time_ns_L,
			postcoh.end_time_V,
			postcoh.end_time_ns_V,
			postcoh.bankid,
			postcoh.tmplt_idx
		FROM
			sim_inspiral
			JOIN sim_coinc_map ON (
				sim_inspiral.simulation_id == sim_coinc_map.sim_id
			)
			JOIN postcoh ON (
				postcoh.event_id == sim_coinc_map.coinc_event_id
			)
	""")
	cursor.execute("""
	SELECT * from sim_sngl_far""")
	with open(name, 'w') as out_csv:
		csvWriter = csv.writer(out_csv)
		for row in cursor:
			csvWriter.writerow(row)
	#rows = cursor.fetchall()

#
# =============================================================================
#
#
# =============================================================================
#
filename = sys.argv[1]
output = sys.argv[2]
#output = "something"
tmp_space = "_CONDOR_SCRATCH_DIR"
verbose = True
working_filename = dbtables.get_connection_filename(filename, tmp_path = tmp_space, verbose = verbose)
connection = sqlite3.connect(working_filename)
create_sim_coinc_view(connection, output)
connection.close()
dbtables.discard_connection_filename(filename, working_filename, verbose = verbose)


