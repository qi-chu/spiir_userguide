segs=`ls 000/H1L1* |grep SEG | grep 59990`
mkdir -p segments_data
mkdir -p /home/spiir/public_html/O3
mkdir -p /home/spiir/public_html/O3/segs
for this_seg in $segs
do
	start=`echo $this_seg |cut -d_ -f 3`
	duration=`echo $this_seg |cut -d_ -f 4|cut -d. -f 1`
	outdir=segments_data/${start}_${duration}
	webdir=/home/spiir/public_html/O3/segs/${start}_${duration}
	mkdir -p $outdir
	mkdir -p $webdir
	gstlal_plot_segments --segments-file $this_seg --segments-name postcohprocessed --output-dir $outdir --webserver-dir $webdir
done
