dir=/fred/oz016/jbosveld/sbnk/gstlal_iir_bank_split
macrofarinput="000/000_marginalized_stats_2w.xml.gz,000/000_marginalized_stats_1d.xml.gz,000/000_marginalized_stats_2h.xml.gz" 
macrojobtag="000" 
macronodename="postcohspiir" 
macrochannel="H1=GDS-CALIB_STRAIN --channel-name L1=GDS-CALIB_STRAIN --channel-name V1=Hrec_hoft_16384Hz" 
macrodqchannel="H1=DMT-DQ_VECTOR --dq-channel-name L1=DMT-DQ_VECTOR --dq-vector-on-bits H1=7 --dq-vector-on-bits L1=7 --dq-vector-off-bits H1=0 --dq-vector-off-bits L1=0" 
macrostate="H1=GDS-CALIB_STATE_VECTOR --state-channel-name L1=GDS-CALIB_STATE_VECTOR --state-channel-name V1=DQ_ANALYSIS_STATE_VECTOR --state-vector-on-bits H1=482 --state-vector-on-bits L1=482 --state-vector-on-bits V1=482 --state-vector-off-bits H1=0 --state-vector-off-bits L1=0 --state-vector-off-bits V1=0"
 
 macroiirbank="H1:${dir}/iir_H1-GSTLAL_SPLIT_BANK_0003-a1-0-0.xml.gz,L1:${dir}/iir_L1-GSTLAL_SPLIT_BANK_0003-a1-0-0.xml.gz,V1:${dir}/iir_V1-GSTLAL_SPLIT_BANK_0003-a1-0-0.xml.gz" macrostatsprefix="000/bank18_stats" macrooutprefix="000/000_zerolag"
psd="H1L1V1-REFERENCE_PSD-1186624818-511200.xml.gz"
frame_cache=/fred/oz016/data/frame.cache.C00 


# no state, no dq, fir whitening
GST_DEBUG_DUMP_DOT_DIR=. gstlal_inspiral_spiir --gps-start-time 1187008782 --gps-end-time 1187009082 --job-tag ${macrojobtag} --tmp-space _CONDOR_SCRATCH_DIR --iir-bank ${macroiirbank} --data-source frames --frame-cache $frame_cache --channel-name ${macrochannel} --gpu-acc multiratespiir --ht-gate-threshold 15.0 --track-psd --psd-fft-length 4 --write-pipeline perf_pipe #--gst-debug cohfar_accumbackground:5 --gst-debug-no-color 
