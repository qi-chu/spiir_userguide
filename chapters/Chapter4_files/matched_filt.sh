bankid=0
for ifo in H1
do
datafile=data_${ifo}_tmplt${bankid}.txt
templatefile=${ifo}_tmplt${bankid}_whitened_fir_template.txt
gstlal_matched_filter --template-fname $templatefile --data-fname $datafile -v --snr-prefix snr_fir_${ifo} --data-prefix data_fir

templatefile=${ifo}_tmplt${bankid}_whitened_spiir_template.txt
gstlal_matched_filter --template-fname $templatefile --data-fname $datafile -v --snr-prefix snr_spiir_tmplt_${ifo} --data-prefix data_spiir_tmplt
templatefile=${ifo}_tmplt${bankid}_whitened_spiir_reconstructed.txt
gstlal_matched_filter --template-fname $templatefile --data-fname $datafile -v --snr-prefix snr_spiir_approx_${ifo} --data-prefix data_spiir_rec
done
