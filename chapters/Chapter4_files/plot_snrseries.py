import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from matplotlib import rc
import numpy
import pdb

rc('text', usetex = True)
mydir = "."
event_time = 1187008880
bankid = 3
tmpltid = 806
pyplot.figure()
fig,axs = pyplot.subplots(3, 1)
fig.subplots_adjust(hspace = .5)
	
for (i, ifo) in enumerate(['H1', 'L1', 'V1']):
	snr = numpy.loadtxt("%s/snr_gpu_%d_%s_%d.dump" % (mydir, event_time, ifo, bankid))

	if (len(snr) > 0):
		time = snr[:,0]
		abssnr = numpy.sqrt(snr[:,tmpltid*2+1]**2 + snr[:, tmpltid*2+2]**2)

		idx_start = min(numpy.where((abs(abssnr)) >0)[0])
		idx_end = max(numpy.where((abs(abssnr)) >0)[0])
		time = time[idx_start:idx_end]
		abssnr = abssnr[idx_start:idx_end]
		print "max snr", max(abssnr)
		axs[i].set_ylabel('%s snr' % ifo)
		axs[i].plot(time, abssnr)

output="%s/snr_%d_%d.png" % (mydir, event_time, bankid)
pyplot.savefig(output)


