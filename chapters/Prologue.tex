\chapter{Introduction}

SPIIR is a software routine (pipeline) used to search for gravitational wave signals from data of laser interferometers. It has been experimented in the LIGO-Virgo first observing run (O1) and the second observing run (O2). For the third observing run (O3) run, it was selected as one of the five online pipelines to generate public alerts during O3\footnote{\url{https://emfollow.docs.ligo.org/userguide/}}. The latest description of the pipeline can be found below\footnote{\url{https://arxiv.org/pdf/2011.06787v2.pdf}}. The code review processes, observing run results and development plans for SPIIR can be found at the internal LIGO Scientific Collaboration (LSC) website\footnote{\url{https://wiki.ligo.org/CBC/Searches/SPIIR/}}.

This document serves as a technical reference of the software base, installation, and examples to use the SPIIR pipeline.

\section{User requirements}
To understand this document, it is desired that readers possess basic knowledges of linux/mac terminals, computing languages including C, python, and Makefile. Understanding of supercomputing, GPU acceleration and CUDA language is preferred.

\section{Access of this document and supporting material}

The source files for this document are currently hosted at on git.ligo.org\footnote{\url{https://git.ligo.org/qi-chu/spiir_userguide}} and would be migrated to github. The latest PDF version of the document can be viewed directly using the link shown at the first page of the repository. Alternatively you can use the following command to download all the source files:
\begin{lstlisting}
git clone https://git.ligo.org/qi-chu/spiir_userguide.git spiir_userguide
\end{lstlisting}
The entry source file main.tex can be compiled using a Tex software, e.g. texshop, for generation of the PDF version of the document. 

The scripts for all the programs listed in this document are included in the repository. You can check out the text file "chapters/Chapterx.tex" for each chapter to locate the scripts used in the corresponding text.

\section{Contributors}
Main contributors for the SPIIR package are from the GW data analysis group of UWA and collaborators from Tsinghua Univ., and Caltech, including (might miss a few):
\begin{center}
 \begin{tabular}{|c|c|c|} 
 \hline
Shaun Hooper & UWA & 2010 - 2013 \\
Qi Chu & UWA & 2012- 2021 \\
Linqing Wen & UWA  & 2010 - \\
Manoj Kovalam & UWA & 2018 - \\
Fiona Panther & UWA & 2021 - \\
Yin Zhong & UWA & 2021 - \\
Alex Codoreanu & GWDC & 2021 - \\
Patrick Clearwater & GWDC & 2021 - \\
Yuan Liu & Tsinghua & 2012 \\
Xiangyu Guo & Tsinghua & 2016 \\
Xiaoyang Guo & Tsinghua & 2017 \\
\hline
\end{tabular}
\end{center}

Should you want to be a contributor to the SPIIR pipeline/package described by this document or to be a contributor to this document, please contact linqing.wen@uwa.edu.au or fiona.panther@uwa.edu.au. 

\section{Pipeline codebase}

The codebase for the SPIIR pipeline (refers to the SPIIR package throughout the document) bears the name \verb;gstlal-spiir; in the LSC software code repository\footnote{\url{https://git.ligo.org/lscsoft/spiir}}. The repository also contains the \verb;gstlal;, \verb;gstlal-ugly;, and \verb;gstlal-inspiral; packages this SPIIR package relies on. As the SPIIR package is not updated to the latest \verb;gstreamer-1.0; standard, these legacy \verb;gstlal;  packages are kept for dependency. Once the SPIIR package has upgraded to \verb;gstreamer-1.0;, these dependent packages can be removed and instead we can use the latest \verb;gstlal; libraries below\footnote{\url{https://git.ligo.org/lscsoft/gstlal}}. 

The SPIIR pipeline is constructed in the SPIIR package using modules from the SPIIR package but also from the \verb;gstlal; packages and \verb;gstreamer; library. \verb;gstlal; is an open-source low-latency tool library developed for the gravitational wave (GW) data analysis group and together with \verb;gstlal-ugly; and \verb;gstlal-inspiral; is the codebase for the GstLAL pipeline. The name \verb;gst; of \verb;gstlal; represents the multimedia software library --- \verb;gstreamer;\footnote{\url{https://gstreamer.freedesktop.org/}} and the name \verb;lal;\footnote{\url{https://git.ligo.org/lscsoft/lalsuite}} is from the GW analysis software library --- the LIGO Algorithm Library (LAL) suite. 

\subsection{Gstreamer framework for online data pipeline}
The technical term "pipeline" here refers to a processing routine that directly takes data in and output useful information after several processing modules. The \verb;gstreamer; framework is used here to build the infrastructure of the SPIIR pipeline. First and foremost, data is represented in this framework as \textit{buffer}s, which is a structure consists of an arbitrary length of time-series or multi-dimension of data with meta information. Meta-information usually includes the duration, the start timestamp, the number of samples, the precision type and other properties of the data. 

\textit{buffer}s are taken in and processed in functional units called \textit{elements} or \textit{plugins}. There are many different types of elements. The most popular types are the Transform type (one input buffer, one output buffer), e.g, the cuda\_multiratespiir element; the Collected type (multiple input buffers, one output buffer), e.g. cuda\_postcoh element. One can use ``gst-inspect" command on a terminal to see how many elements are installed and use ``gst-inspect cuda\_multiratespiir" to see the accepted input and out data formats and the required parameters for that element. %We refer readers to~\cite{gstreamer_doc} for a complete documentation of this library. 

A \textit{pipeline} is made by linking a series of \textit{elements} like what we use LEGO modules to build anything. There are two basic requirements for a pipeline to work. The first requirement is that the data formats that adjacent elements can process need to be compatible. The other is that there needs to be an element to trigger the pipeline processing --- this element is usually the first element that handles the data interface. The linking code is normally written in Python. For example, the SPIIR pipeline executable: gstlal\_inspiral\_postcohspiir\_online.

When a pipeline starts, \textit{buffer}s will one by one flow through \textit{elements}. The data flow is managed by a higher level \textit{event} schedule mechanism. This \textit{event}, different from a GW event, is a \verb;gstreamer; function that notifies specific elements to start running.

To achieve real-time processing, the processing time of a data \textit{buffer} in an \textit{element} needs to be less than the duration of the \textit{buffer}, to avoid the latency accumulation. Currently we are only able to measure the overall latency of the pipeline as shown in the paper. With the package upgrade to \verb;gstreamer;, it is expected the latency for each \textit{element} can be measured easily. %The latency of the SPIIR pipeline is not fixed, as the computation of the coincidence or coherent processing component varies with the noise behavior of the data. The estimated latency distribution over the main pipeline components is presented in Tab.~\ref{tab:pipe_latency}.

\iffalse
\subsection{Package directory tree}
Here we show the directory of the software package that contains the construction of the offline and online pipeline and the code for SPIIR filtering and coherent analysis. The source files can be browsed at: \href{https://git.ligo.org/lscsoft/spiir}.
(The source files are under review and subject to change.)

The directory is \verb;gstlal-spiir;:
\dirtree{%
 .1 gstlal-spiir.
 .2 bin.
 .3 gstlal\_inspiral\_postcohspiir\_online $\heartsuit$.
 .3 gstlal\_inspiral\_postcohspiir\_offline $\heartsuit$.
 .3 gstlal\_inspiral\_postcohspiir\_lvalert\_background\_plotter.
 .3 gstlal\_cohfar\_plot\_ifar $\clubsuit$.
 .3 gstlal\_cohfar\_skymap2fits.
 .2 gst.
 .3 cohfar.
 .4 background\_stats.h $\clubsuit$.
 .4 background\_stats\_utils.c $\clubsuit$.
 .4 background\_stats\_utils.h $\clubsuit$.
 .4 cohfar\_accumbackground.c $\clubsuit$.
 .4 cohfar\_accumbackground.h $\clubsuit$.
 .4 cohfar\_assignfar.c $\clubsuit$.
 .4 cohfar\_assignfar.h $\clubsuit$.
 .4 ssvkernel.c $\clubsuit$.
 .4 ssvkernel.h $\clubsuit$.
 .4 test.
 .3 cuda.
 .4 spiir.
 .5 spiir.c $\clubsuit$.
 .5 spiir.h $\clubsuit$.
 .5 spiir\_kernel.cu $\clubsuit$.
 .5 spiir\_kernel.h $\clubsuit$.
 .4 multiratespiir.
 .5 multiratespiir.c $\clubsuit$.
 .5 multiratespiir.h $\clubsuit$.
 .5 multiratespiir\_kernel.cu $\clubsuit$.
 .5 multiratespiir\_kernel.h $\clubsuit$.
 .5 multiratespiir\_utils.c $\clubsuit$.
 .5 multiratespiir\_utils.h $\clubsuit$.
 .5 test.
 .4 postcoh.
 .5 postcoh\_utils.c $\clubsuit$.
 .5 postcoh\_utils.h $\clubsuit$.
 .5 postcohinspiral\_table\_utils.c $\clubsuit$.
 .5 postcohinspiral\_table\_utils.h $\clubsuit$.
 .5 postcohinspiral\_table.h $\clubsuit$.
 .5 postcoh.c $\clubsuit$.
 .5 postcoh.h $\clubsuit$.
 .5 postcoh\_kernel.cu $\clubsuit$.
 .5 postcoh\_kernel.h $\clubsuit$.
 .5 postcoh\_filesink.c $\clubsuit$.
 .5 postcoh\_filesink.h $\clubsuit$.
 .5 test.
 .3 spiir.
 .4 spiir.c $\clubsuit$.
 .4 spiir.h $\clubsuit$.
 .2 python.
 .3 postcoh\_finalsink.py $\clubsuit$.
 .3 postcoh\_table\_def.py $\clubsuit$.
 .3 spiirparts.py $\heartsuit$.
}

\fi
