\chapter{Pipeline operation}

The complete SPIIR coherent pipeline performs the SPIIR filtering function as explained in the last chapter, and also applies coherent searches over SNR series of individual detectors. And it then selects candidates from the coherent triggers and assign significances to the candidates. This chapter provides the flowcharts in Sec.~\ref{sec:flowchart} and codebase for the complete SPIIR pipeline in Sec.~\ref{sec:codebase}. Sec.~\ref{sec:examples} gives examples to set up the pipeline in an offline configuration and an online configuration. 

\section{The complete SPIIR pipeline}


\subsection{Flowchart}
\label{sec:flowchart}

Fig.~\ref{fig:online_newrank} shows the flowchart of the complete SPIIR pipeline. For more explanations of the flowchart, the reader is refered to Chapter 5 of Qi Chu's PhD thesis.

\begin{figure}[!h]
%\begin{subfigure}{.6\textwidth}
%\hspace*{-0.5in}
\centering
\includegraphics[width=.5\textwidth]{chapters/Chapter5_files/online_3det_newrank.eps}
\caption[Flowchart of an online 3-det SPIIR newrank pipeline.]{Flowchart of an online 3-det SPIIR newrank pipeline. Modules marked in blue are implemented on GPU. Modules highlighted in red are undergoing changes for the new ranking statistic.}
\label{fig:online_newrank}
%\end{subfigure}
%\caption{Comparison of SPIIR coherent pipelines. Modules marked in shadow are implemented on GPU.}
\end{figure}


\subsection{Codebase}
\label{sec:codebase}
Here the master branch (\href{https://git.ligo.org/lscsoft/spiir/tree/master}{code repository}) is selected to be shown as it is the most stable developing branch for the SPIIR project. There are several development branches for different new features on a few elements and they bear the same flowchart. The master branch uses existing \textsf{gstlal} components for the first few processing steps. Tab.~\ref{tab:whole_codebase} lists the modules from the flowchart and corresponding implementations in \textsf{gstlal} plugin or in SPIIR plugin. 

\begin{table*}[ht]
\caption{SPIIR pipeline and corresponding \textsf{gstlal} or SPIIR software plugins. Each plugin is implemented in several code files in C langauage or Python language listed in the last column. The pipeline executable code is \textsf{gstlal\_inspiral\_postcohspiir\_online}.}
\scriptsize
%\resizebox{0.8\textwidth}{!}{
\begin{tabular}{|p{4cm}|p{3cm}|p{3cm}|p{3cm}|}
\hline
SPIIR pipeline modules & gstlal plugin & SPIIR plugin & code\\
\hline
Read from shared memory & gds\_lvshmsrc, framecpp\_channeldemux & & \\
\hline
Apply state\_vector, DQ channel, & lal\_statevector, lal\_gate & & \\
\hline
Data whitening & lal\_whiten, lal\_tdwhiten & & \\
\hline
Gating & lal\_gate & & \\
\hline
Multirate SPIIR bank filtering & & cuda\_multiratespiir & spiirparts.py, multiratespiir.c, multiratespiir.h, multiratespiir\_kernel.cu, multiratespiir\_utils.h, multiratespiir\_utils.c\\
\hline
Coherent post-processing with sky localization and Time-shifted events & & cuda\_postcoh & spiirparts.py, postcoh.c, postcoh.h, postcoh\_kernel.cu, postcoh\_utils.c, postcoh\_utils.h\\
\hline
FAR estimation from background events & & cohfar\_accumbackground & spiirparts.py, cohfar\_accumbackground.c, cohfar\_accumbackground.h, background\_stats.h, background\_stats\_utils.c, background\_stats\_utils.h, knn\_kde.c, knn\_kde.h \\
\hline
Assign FAR to current foreground events & & cohfar\_assignfar &spiirparts.py, cohfar\_assignfar.c, cohfar\_assignfar.h, cohfar\_calc\_fap.c \\
\hline
Cluster foreground events, FAR consistency veto, and trigger submission & & postcoh\_finalsink & postcoh\_finalsink.py \\
\hline
\end{tabular}
%}
\label{tab:whole_codebase}
\end{table*}


\section{Examples}
\label{sec:examples}

\subsection{Offline search for GW170817}
\label{sec:pipe_offline}

The following script shows the setup of the pipeline to search over a period of offline data around the GW170817 event time (GPS:1187008882). This script can be found at OzStar \\ (/fred/oz016/chichi/unit\_test/test\_gw170817\_userguide/run\_gw170817\_offline.sh). You can copy the example over and execute it in your own directory. The flowchart can be found in \href{https://git.ligo.org/lscsoft/spiir/tree/spiir_userguide/v0.1/chapters/Chapter5_files/offline_gw170817_trig.png}{this link}.

\lstinputlisting[language=Bash]{chapters/Chapter5_files/run_gw170817_offline.sh}

The explanation for the pipeline settings are the following:

\lstinputlisting[language=HTML]{chapters/Chapter5_files/exp_gw170817_offline.html}

The outputs of this offline run include:
\begin{enumerate}
\item 000/000\_zerolag\_1187008782\_296.xml.gz: a xml file containing a list of candidates found by this run using bank0 and bank1. You can use "ligolw\_print" to print the candidate information.
\item 000/bank0\_stats\_1187008782\_296.xml.gz, 000/bank1\_stats\_1187008782\_296.xml.gz: two xml files, each includes histograms of the individual SNR and $\xi^2$ distributions and coherent distributions from background events of each bank. These histograms will be used to calculate the individual FARs and the FARs for coherent triggers.
\item 000/H1L1V1\_SEGMENTS\_1187008782\_296.xml.gz: a xml file containing the valid periods (segments) of data that have been processed.
\item 000/finalsink\_debug\_log: a log file to show the status of seperate side programs, other than the pipeline program, which includes the side program of merging background distributions and the side program of marginalizing background files.
\item *\_skymap folders: locations to store localization skymaps generated from this run. Each skymap is corresponding to one trigger. The name of each skymap indicates the trigger information: triggering detector, end time of the triggering detector in GPS seconds, end time of the triggering detector in GPS nanoseconds, bank ID, and template ID. It includes a coherent SNR matrix and a null SNR matrix in binary format.
\item trigger\_control.txt: a text file that lists the candidates from all pipeline jobs that have or have not passed the final checks. Each row corresponding to one candidate. It reports 3 aspects of a given candidate: first the end time of the candidate in GPS seconds, second the FAR, and last to be submitted to GraceDB (marked as 1) or not (marked as 0).
\end{enumerate}


\subsection{Online pipeline setup}

The following script shows the setup of the pipeline to search over real-time live data. Note that at this moment, only Tier-1 clusters of LIGO-Virgo have real-time live data from the LIGO and Virgo detectors.

\lstinputlisting[language=Bash]{chapters/Chapter5_files/run_online.sh}

The differences between the online configuration with the offline configuration mainly include 3 aspects. First the data reading settings. The offline configuration reads from the hard disk, it reads not only data in frame format, but also the frame-segment file and the veto-segment file which mark the quality of the data. Those two files are generated after data has been collected. The online configuration reads from the cache memory of the Tier-1 center and uses the two data quality channels for quality check. Secondly the offline uses an offline PSD for whitening which is estimated of the entire analyzed period. The online configuration uses the PSD estimated on the fly. Lastly, the online pipeline uses the background files on the fly. The outstanding settings for the online configuration are shown here:

\lstinputlisting[language=HTML]{chapters/Chapter5_files/exp_online.html}


\subsection{Pipeline post-processing}

\subsubsection{Processing candidates}

In a common online or offline configuration, candidates from the pipeline are dumped every half day for each job. So for a run over a week or longer, sometimes it is necessary to combine candidates from different jobs or from consecutive times for bulk analysis. We found the best practice is to combine results of all jobs for one day. Beyond that, the end product would be too large, over serveral GBs, to analyze.

The program "ligolw\_sqlite\_postcoh" is used to merge candidates in xml files into one sqlite database. It is actually not limited to merge candidates, it can recognize any tables in xml files and combine them. It is used to combine candidate xmls, a segment xml, and an injection xml in a detection analysis. See an example script below:

\lstinputlisting[language=Bash]{chapters/Chapter5_files/combine_triggers.sh}

The sqlite database can be converted back to a xml file using the same program "ligolw\_sqlite\_postcoh" with the option "--extract". An example script is shown below:

\lstinputlisting[language=Bash]{chapters/Chapter5_files/extract_sql.sh}

The program "ligolw\_inspinjfind\_postcoh" will find candidates of the "postcoh" table which are in vicinity with any injecitons of the "sim\_inspiral" table in a xml file. It will append a "coinc\_event\_map" table to the xml file which gives the postcoh:event\_id and the sim\_inspiral:simulation\_id for any given match. This program can only process xml files. Consider processing sql database directly, instead of processing xml converted from the sql database, in the future. This program needs a window setting in seconds indicating the range to search around an injection time. Here is an example:

\lstinputlisting[language=Bash]{chapters/Chapter5_files/find_inj.sh}

\subsubsection{Plotting data segment information}

The duty cycles of a LIGO detector is not 100\%, but rather around 50\% to 80\% in the latest O3 run. At certern times, data from any detector are not usable either due to incurred environmental noises or suspension due to maintenance. The program "gstlal\_plot\_segments" is used to plot periods of data that we think are valid and thus may be applied slight paddings at the edges for second by second processing. The input is the segment xml output of an online or offline run (e.g. 000/H1L1V1\_SEGMENTS\_1187008782\_296.xml.gz in the last section). This command needs an environment parameter to be set for the html webpage style which can display the period information in color. That parameter is currently only availble on the CIT cluster. The following example can be executed on CIT and the outputs are saved in two folders, one folder in the execution directory giving a list of segments and the other is in the public\_html directory which gives a webpage displaying processed periods in color as shown in Fig.~\ref{fig:segs_example}.

\lstinputlisting[language=Bash]{chapters/Chapter5_files/plot_segs.sh}


\subsubsection{Summarizing and plotting candidates}

"gstlal\_inspiral\_postcohspiir\_plotsummary" is the program that can output 10 sets of plots or tables given a result database of an injection run, or that of an non-injection run, or that of a shifted data run, or any combinations of the above. If input includes a database from an non-injection run, it will output one SegmentsTable, one SummaryTable, one set of RateVSThreshold (e.g. IFAR) plots, one set of BackgroundPlots plots. If the input includes a injection run result, it will output one set of MissFound plots, one set of ParameterAccuracy plots, one set of InjectionParameterDistributionPlots plots, and one set of ROCPlots plots. If the input includes a result from a shifted run, it will output one SegmentsTable, one SummaryTable, and one set of RateVSThreshold (e.g. IFAR) plots. The shifted result is normally used for verification of the FAR estimation by testing if the IFAR curve agrees with the expectation, the so called closed-box verification. At this moment, the shifted output is not checked yet as our open-box IFAR curve matches the expectation well when signals are absent.

The most important figure that demonstrate the detection capability of a pipeline is the MissFound plot. It shows the distribution of missed injections and found injections in terms of distances and source chirp masses. Here is an example \href{https://ldas-jobs.ligo.caltech.edu/~spiir/review/bns_set2_gstlalinj_clean/2det_gstlalpsdwhiten/dailypage/1186642720/#injection_recovery}{here}.

\subsection{Bulk analysis}

The best practice so far is to dump candidates daily and post-process them and plot them as mentioned above. This is due to technical limit of the xml or sqlite file size of candidates. If you want to do analysis over a chunk of data, such as one week or 10 days which are typical divisions for LIGO catalog searches, the current solution is to dump the sql file with your interested features into a normal text file and process from there. FIXME, consider extending "gstlal\_inspiral\_postcohspiir\_plotsummary" to deal with multiple daily databases.

The script to select detections with interested parameters (e.g. SNR, detected end time) and dump them to a local file is given below at Chapter5\_files/dump\_sim\_coinc\_spiir.py. An example script to read the dumped features from all daily databases and plot several plots is given in the attached folder Chapter5\_files with the name trace\_det.py

%\lstinputlisting[language=python]{chapters/Chapter5_files/dump_sim_coinc_spiir.py}

\begin{figure}[!h]
%\begin{subfigure}{.6\textwidth}
%\hspace*{-0.5in}
\centering
\includegraphics[width=.9\textwidth]{chapters/Chapter5_files/segs.png}
\caption[]{An example of the output of the gstlal\_plot\_segments program, displaying data periods that are processed by the SPIIR pipeline of a run. This plot is a snapshot from \href{https://ldas-jobs.ligo.caltech.edu/~spiir/O3/segs/1252059990_43200/}{this web link} and the web link can only viewed by LIGO members.}
\label{fig:segs_example}
%\end{subfigure}
%\caption{Comparison of SPIIR coherent pipelines. Modules marked in shadow are implemented on GPU.}
\end{figure}



\section{Pipeline versions for O3}
The production run of SPIIR in O3 uses versions in branch spiir-review-O3. The latest version is spiir-O3-v5. More details can be found in document \href{https://git.ligo.org/lscsoft/spiir/tree/spiir_userguide/review_newrank}{here}. 
