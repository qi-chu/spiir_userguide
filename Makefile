main.pdf:main.tex
	latexmk -pdf -f $<

clean:
	rm -f main.{aux,bbl,blg,fdb_latexmk,fls,log,out,pdf}
